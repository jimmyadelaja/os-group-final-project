#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <pthread.h>
#include <assert.h>

typedef struct __node_t {
 int value;
 struct __node_t *next;
 } node_t;

 typedef struct __queue_t {
 node_t *head;
 node_t *tail;
// pthread_mutex_t headLock;
// pthread_mutex_t tailLock;
 } queue_t;


queue_t Queue_Init(queue_t q);
	void Queue_Enqueue(queue_t *q, int value);
	int Queue_Dequeue(queue_t *q);
	void Queue_runlist(queue_t *list);
	void Queue_print(queue_t *list);
	void *run_list(void *arg);
	
int main(int argc, char **argv){
	
	queue_t my_a;
		queue_t my_q=Queue_Init(my_a);
		
		queue_t *my_b = &my_q;
		
	
		//Queue_runlist(my_b);
		
		
		
		pthread_t t1;
		pthread_t t2;
		
		pthread_create(&t1,NULL,run_list,&my_q);
		pthread_create(&t2,NULL,run_list,&my_q);
		pthread_join(t1,NULL);
		pthread_join(t2,NULL);
		
		Queue_print(my_b);
	
	
return 0;
}



	queue_t Queue_Init(queue_t q) {
	 node_t *tmp = malloc(sizeof(node_t));
	 tmp->next = NULL;
	 q.head = q.tail = tmp;
	/* q->head=NULL;
	 q->tail=NULL; */
	// pthread_mutex_init(&q.headLock, NULL);
	// pthread_mutex_init(&q.tailLock, NULL);
	 return q;
	 }

	 void Queue_Enqueue(queue_t *q, int value) {
	 node_t *tmp = malloc(sizeof(node_t));
	 assert(tmp != NULL);
	 tmp->value = value;
	 tmp->next = NULL;

	// pthread_mutex_lock(&q->tailLock);
	 q->tail->next = tmp;
	 q->tail = tmp;
	// pthread_mutex_unlock(&q->tailLock);
	 }

		 int Queue_Dequeue(queue_t *q) {
	// pthread_mutex_lock(&q->headLock);
	 node_t *tmp = q->head;
	 node_t *newHead = tmp->next;
	 if (newHead == NULL) {
	// pthread_mutex_unlock(&q->headLock);
	 return -1; // queue was empty
	 }
	 //*value = newHead->value;
	 q->head = newHead;
	// pthread_mutex_unlock(&q->headLock);
	 free(tmp);
	 return 0;
	 }
	 
	 void Queue_runlist(queue_t *list){
		//list_t *list = (list_t *) arg;
		Queue_Enqueue(list,3);
		Queue_Enqueue(list,5);
		Queue_Enqueue(list,8);
		Queue_Enqueue(list,9);
		//return 0;
		}
	void Queue_print(queue_t *list){
		node_t *curr = list->head;
		while (curr){
			printf("%d\n",curr->value);
			curr = curr->next;
			}
		}
		
	void *run_list(void *arg){
	queue_t *list = (queue_t *) arg;
	Queue_Enqueue(list,3);
	Queue_Enqueue(list,5);
	Queue_Enqueue(list,8);
	Queue_Enqueue(list,9);
	Queue_Dequeue(list);
	//return NULL;
	}