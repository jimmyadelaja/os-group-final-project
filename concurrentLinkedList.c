//This is the implementation of a concurrent linked list
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <pthread.h>

//node_t is a structure object for a node in the list
typedef struct __node_t{
	int key;
	struct __node_t *next;
	} node_t;

//list_t is a structure object that is used to represent the linked list
typedef struct __list_t{
	node_t *head;
	pthread_mutex_t lock;
	} list_t;

//list_init() is used to initialize the linked list
list_t list_init(list_t L){
	L.head = NULL;
	pthread_mutex_init(&L.lock,NULL);
	return L;
	}

//list_insert() is used to insert items in the linked list structure created
list_t *list_insert(list_t *L, int key){
	pthread_mutex_lock(&L->lock);
	node_t *new = malloc(sizeof(node_t));
	if(new == NULL){
		perror("malloc");
		pthread_mutex_unlock(&L->lock);
		}
	new->key = key;
	new->next = L->head;
	L->head = new;
	pthread_mutex_unlock(&L->lock);
	return L;
	}

//the delete() function is meant to delete the first item in the linked list
void list_delete(list_t *L){
   pthread_mutex_lock(&L->lock);
   node_t *tmp = L->head;
   L->head = tmp->next;
   pthread_mutex_unlock(&L->lock);
}
//list_lookup() is used for finding a particular value which may exist in the linked list
int list_lookup(list_t L, int key){
	pthread_mutex_lock(&L.lock);
	node_t *curr = L.head;
	while (curr){
		if(curr->key == key){
			printf("Found it!");
			pthread_mutex_unlock(&L.lock);
			return 0;
			}
			curr = curr->next;
		}
		pthread_mutex_unlock(&L.lock);
		return -1;
	}

void print_list(list_t *L){
	node_t *curr = L->head;
	while (curr){
		printf("%d\n",curr->key);
		curr = curr->next;
		}
	}

void *run_list(void *arg){
	list_t *list = (list_t *) arg;
	printf("printing the insertion of 3,5,8 and 9 into the linked list by both threads\n");
	list_insert(list,3);
	list_insert(list,5);
	list_insert(list,8);
	list_insert(list,9);
	print_list(list);
	printf("printing the deletion of the first element in the linked list by both threads\n");
	list_delete(list);
	print_list(list);
	return NULL;
	}


int main(int argc, char *argv[]){
	//instantiate a linked list
	list_t Linked_list;
	Linked_list = list_init(Linked_list);
	//create two threads and run them concurrently
	pthread_t t1;
	pthread_t t2;
	//initialize the instantiated linked list
	pthread_create(&t1,NULL,run_list,&Linked_list);
	pthread_create(&t2,NULL,run_list,&Linked_list);
	pthread_join(t1,NULL);
	pthread_join(t2,NULL);

	//print_list(Linked_list);


	return 0;

	}
