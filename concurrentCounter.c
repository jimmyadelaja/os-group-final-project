#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <pthread.h>

// Code for concurrent counter
typedef struct __counter_t{
    int value;
    pthread_mutex_t lock;
} counter_t;

counter_t init(counter_t c){
    c.value = 0;
    pthread_mutex_init(&c.lock, NULL);
    return c;

}

void *increment(counter_t *c){
    pthread_mutex_lock(&c->lock);
    c->value++;
    pthread_mutex_unlock(&c->lock);
}

void decrement(counter_t *c){
    pthread_mutex_lock(&c->lock);
    c->value--;
    pthread_mutex_unlock(&c->lock);
}

int get(counter_t c){
    pthread_mutex_lock(&c.lock);
    int rc = c.value;
    pthread_mutex_unlock(&c.lock);
    return rc;
}

// Function that increments the value of counter_t struct
void *count (void *arg){
    counter_t *counter = (counter_t *) arg;
    int i;
    int end = 10000;
    for (i=0;i<end;i++){
        increment(counter);
    }
    return NULL;
}

// Main function to test concurrent counter
int main(){
    //initialized a counter
    counter_t my_counter;
    my_counter = init(my_counter);

    // declare threads
    pthread_t p1,p2;

    printf("Initial value : %d\n", get(my_counter));

    pthread_create(&p1, NULL, count, &my_counter);
    pthread_create(&p2, NULL, count, &my_counter);
    pthread_join(p1, NULL);
    pthread_join(p2, NULL);

    printf("Final value : %d\n", get(my_counter));
    return 0;
}
