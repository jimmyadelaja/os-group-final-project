//#include "concll.h"
//#include "concht.h"
//#include "concq.h"
//#include "concctr.h" 
#include <stdio.h>
#include <sys/wait.h>
#include <pthread.h>
#define BUCKETS (101)


typedef struct __hash_t
{
	int lists[BUCKETS];
} hash_t;

hash_t Hash_init(hash_t H) {
	int i;
	for (i = 0; i < BUCKETS; i++)
	{
		H.lists[i] = NULL;
	}
	return H;
}

hash_t *Hash_Insert(hash_t *H, int key) {
	int bucket = key % BUCKETS;
	H->lists[bucket] = key;
	printf("Item: %d, Bucket: %d\n", H->lists[bucket], bucket);
	//return 0;
	return H;
}

int Hash_Lookup(hash_t H, int key) {
	return key % BUCKETS;
}

hash_t *Hash_Delete(hash_t *H, int key) {
	int bucket = key % BUCKETS;
	H->lists[bucket] = NULL;
	return H;
}

void Hash_Print(hash_t H) {
	for (int i = 0; i < BUCKETS; i++) {
		printf("%dth element: %d\n", i, H.lists[i]);
	}
}


//Concurrent Hash Table - Tobi
void *hTableF1(void *arg) {
	hash_t *HashP = (hash_t *)arg;
	srand(time(NULL));
	for (int i = 0; i < 30; i++){
		int r = rand() % 500;
		Hash_Insert(HashP, r);
	}
}

void *hTableF2(void *arg) {
	hash_t *HashP = (hash_t *)arg;
	srand(time(NULL));
	for (int i = 0; i < 30; i++){
		int r = rand() % 500;
		Hash_Insert(HashP, r);
	}
}

int main() {
	hash_t HashT;
	HashT = Hash_init(HashT);

	pthread_t p;
	pthread_t q;
	pthread_create(&p, NULL, hTableF1, &HashT);
	pthread_create(&q, NULL, hTableF2, &HashT);

	pthread_join(p, NULL);
	pthread_join(q, NULL);
	Hash_Print(HashT);

	return 0;
}