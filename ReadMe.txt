OS Final Group Project
Memebers: David Sasu, Kelvin Degbotse, Ekiyor Odoko, Oluwatobi Adelaja

Descriptions:
Each file contains either a concurrent or nonconcurrent implementation of a counter, Queue, Linked List and Hash Table
Each file has a main function that creates 2 threads which call functions that modifies the global variable data structure
Each file contains a test case

Instructions:
Compile each file using the command "gcc -pthread -o fileName fileName.c"
Run them using "./fileName" to see the output of the test cases